#!/bin/python3
import os.path

from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes

datadir = "/home/neo/Documents/Python/"
privatekeypath = datadir + "private.pem"
publickeypath = datadir + "public.pem"
passwordpath = datadir + "password.bin"
if (
    not os.path.isfile(privatekeypath)
    or not os.path.isfile(publickeypath)
    or not os.path.isfile(passwordpath)
):
    input("Cliquez sur entrer pour générer les clés")
    private_key = RSA.generate(4096)
    public_key = private_key.publickey()
    # print(private_key.exportKey(format='PEM'))
    # print(public_key.exportKey(format='PEM'))

    with open(privatekeypath, "wb") as prv_file:
        prv_file.write(private_key.exportKey())

    with open(publickeypath, "wb") as pub_file:
        pub_file.write(public_key.exportKey())

    # data = "I met aliens in UFO. Here is the map.".encode("utf-8")
    data = input("Mot de passe : ").encode("utf-8")
    with open(passwordpath, "wb") as file_out:
        recipient_key = RSA.import_key(open(publickeypath).read())
        session_key = get_random_bytes(16)

        # Encrypt the session key with the public RSA key
        cipher_rsa = PKCS1_OAEP.new(recipient_key)
        enc_session_key = cipher_rsa.encrypt(session_key)

        # Encrypt the data with the AES session key
        cipher_aes = AES.new(session_key, AES.MODE_EAX)
        ciphertext, tag = cipher_aes.encrypt_and_digest(data)
        [file_out.write(x) for x in (enc_session_key, cipher_aes.nonce, tag, ciphertext)]
file_in = open(passwordpath, "rb")

private_key = RSA.import_key(open(privatekeypath).read())

enc_session_key, nonce, tag, ciphertext = [
    file_in.read(x) for x in (private_key.size_in_bytes(), 16, 16, -1)
]

# Decrypt the session key with the private RSA key
cipher_rsa = PKCS1_OAEP.new(private_key)
session_key = cipher_rsa.decrypt(enc_session_key)

# Decrypt the data with the AES session key
cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
data = cipher_aes.decrypt_and_verify(ciphertext, tag)
print(data.decode("utf-8"))
