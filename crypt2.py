from simplecrypt import decrypt, encrypt

password = "zfcxwqjnozidxno"
message = "this is a secret message"
with open("private.pem", "r") as private:
    with open("private.pem.crypt", "w") as crypted:
        with open("public.pem", "r") as publickey:
            ciphertext = encrypt(publickey.read(), private.read())
            crypted.write(str(ciphertext))

print(ciphertext)
with open("public.pem", "r") as publickey:
    mystring = decrypt(publickey.read(), ciphertext).decode("utf8")
print(mystring)
