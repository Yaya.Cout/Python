"""Impost csv file to sqlite database."""
import csv
import sqlite3
import sys

csvfilepath = "/media/neo/TOSHIBA EXT/Big data/Web/urldata.csv"
databasepath = "/home/neo/localhost/Recherche2/sites.db"

database = sqlite3.connect(databasepath)

if database.total_changes != 0:
    print("SQLite error")
    sys.exit(1)
cursor = database.cursor()

with open(csvfilepath) as csvfile:
    csvreader = csv.reader(csvfile, delimiter=",")  # , quotechar='')
    # csvlist = []
    for idlist, item in enumerate(csvreader):
        if item[1] == "good":
            # print(idlist, item[0])
            item[0] = "http://" + item[0]
            query = "INSERT OR IGNORE INTO search(url) VALUES(?)"
            cursor.execute(query, (item[0],))

        # csvlist.append(item)

csvfile.close()
database.commit()
database.close()
