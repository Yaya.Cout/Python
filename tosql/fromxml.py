"""Impost xml file to sqlite database."""
import sqlite3
import sys
import os

xmldirpath = "/media/neo/TOSHIBA EXT/Big data/Web/xml/xml/"
databasepath = "/home/neo/localhost/Recherche2/sites.db"

database = sqlite3.connect(databasepath)
files = os.listdir(xmldirpath)

if database.total_changes != 0:
    print("SQLite error")
    sys.exit(1)
cursor = database.cursor()

for idlist, item in enumerate(files):
    itemlist = item.split(".")[:-1]
    itemstr = itemlist[0]
    for i in itemlist[1:]:
        itemstr += "."+i
    # print(itemstr)
    query = "INSERT OR IGNORE INTO search(url) VALUES(?)"
    cursor.execute(query, (itemstr,))

database.commit()
database.close()
