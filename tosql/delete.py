"""Impost csv file to sqlite database."""
import sqlite3
import sys

databasepath = "/home/neo/localhost/Recherche2/sites.db"

database = sqlite3.connect(databasepath)

if database.total_changes != 0:
    print("SQLite error")
    sys.exit(1)
cursor = database.cursor()

query = "DELETE FROM sites"
cursor.execute(query)

database.commit()
database.close()
