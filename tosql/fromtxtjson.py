"""Impost csv file to sqlite database."""

import csv
import sqlite3
import sys

txtfilepath = "/media/neo/TOSHIBA EXT/Big data/Web/csv/success.txt"
databasepath = "/home/neo/localhost/Recherche2/sites.db"

database = sqlite3.connect(databasepath)

if database.total_changes != 0:
    print("SQLite error")
    sys.exit(1)
cursor = database.cursor()

with open(txtfilepath) as txtfile:
    files = txtfile.read()
query = "INSERT OR IGNORE INTO search(url) VALUES(?)"
for idlist, item in enumerate(files):
    print(item)
    cursor.execute(query, (item,))

database.commit()
database.close()
