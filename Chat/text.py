import gi
from gi.repository import Gtk, Pango

gi.require_version("Gtk", "3.0")

msgs = [["coucou, cv", "Imbatable"], ["oui, et toi ?", "Mercenaire"]]


class SearchDialog(Gtk.Dialog):
    def __init__(self, parent):
        Gtk.Dialog.__init__(
            self,
            title="Search",
            transient_for=parent,
            modal=True,
        )
        self.add_buttons(
            Gtk.STOCK_FIND,
            Gtk.ResponseType.OK,
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
        )

        box = self.get_content_area()

        label = Gtk.Label(label="Insert text you want to search for:")
        box.add(label)

        self.entry = Gtk.Entry()
        box.add(self.entry)

        self.show_all()


class TextViewWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="TextView Example")

        self.set_default_size(1000, 500)

        self.grid = Gtk.Grid()
        self.add(self.grid)

        self.create_textview()
        self.create_toolbar()

    def create_toolbar(self):
        toolbar = Gtk.Toolbar()
        self.grid.attach(toolbar, 0, 0, 3, 1)
        button_search = Gtk.ToolButton()
        button_search.set_icon_name("system-search-symbolic")
        button_search.connect("clicked", self.on_search_clicked)
        toolbar.insert(button_search, 11)

    def create_textview(self):
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        self.grid.attach(scrolledwindow, 0, 1, 3, 1)

        self.textview = Gtk.TextView()
        self.textview.set_editable(False)
        self.textview.set_wrap_mode(Gtk.WrapMode.WORD)
        self.textview.set_cursor_visible(False)
        self.textbuffer = self.textview.get_buffer()
        # self.textbuffer.set_text(
        #     "This is some text inside of a Gtk.TextView. "
        #     + "Select text and click one of the buttons 'bold', 'italic', "
        #     + "or 'underline' to modify the text accordingly."
        # )
        scrolledwindow.add(self.textview)

        # self.tag_bold = self.textbuffer.create_tag("bold", weight=Pango.Weight.BOLD)
        # self.tag_italic = self.textbuffer.create_tag("italic", style=Pango.Style.ITALIC)
        # self.tag_underline = self.textbuffer.create_tag(
        # "underline", underline=Pango.Underline.SINGLE
        # )
        self.tag_found = self.textbuffer.create_tag("found", background="yellow")
        self.tag_sended = self.textbuffer.create_tag("send", foreground="green")
        self.tag_received = self.textbuffer.create_tag("received", foreground="yellow")
        start_iter = self.textbuffer.get_start_iter()
        for i in msgs:
            if i[1] == "Imbatable":
                self.textbuffer.insert_with_tags(
                    start_iter, i[0] + "\n", self.tag_sended
                )
                # self.textview.set_justification(Gtk.Justification.LEFT)
            else:
                self.textbuffer.insert_with_tags(
                    start_iter, i[0] + "\n", self.tag_received
                ).set_justification(Gtk.Justification.RIGHT)
                # self.textview.set_justification(Gtk.Justification.RIGHT)

    def on_justify_toggled(self, widget, justification):
        self.textview.set_justification(justification)

    def on_search_clicked(self, widget):
        dialog = SearchDialog(self)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            cursor_mark = self.textbuffer.get_insert()
            start = self.textbuffer.get_iter_at_mark(cursor_mark)
            if start.get_offset() == self.textbuffer.get_char_count():
                start = self.textbuffer.get_start_iter()

            self.search_and_mark(dialog.entry.get_text(), start)

        dialog.destroy()

    def search_and_mark(self, text, start):
        end = self.textbuffer.get_end_iter()
        match = start.forward_search(text, 0, end)

        if match is not None:
            match_start, match_end = match
            print(match)
            self.textbuffer.apply_tag(self.tag_found, match_start, match_end)
            self.search_and_mark(text, match_end)


win = TextViewWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
