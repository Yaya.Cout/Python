#!/bin/python3
import subprocess
import time


def main():
    audioplace = "/home/neo/Téléchargements/Youtube/audio"
    videoplace = "/home/neo/Téléchargements/Youtube/video"
    urlplace = "/home/neo/Documents/youtube_url.txt"
    audiofiletype = "mp3"
    videofiletype = "mp4"
    subprocess.run(
        args=["pip3", "install", "youtube_dl", "--upgrade", "--no-cache-dir"],
        check=True,
    )
    while True:
        with open(urlplace, "r+") as urlfile:
            urllist = urlfile.read().split("\n")
            for url in urllist:
                print(url)
                audioargs = [
                    "youtube-dl",
                    "--add-metadata",
                    "-x",
                    "-q",
                    "--skip-unavailable-fragments",
                    "--console-title",
                    "-i",
                    "-o",
                    "%(title)s.%(ext)s",
                    "--restrict-filenames",
                    "--all-subs",
                    "--audio-format",
                    audiofiletype,
                    url,
                ]

                subprocess.run(cwd=audioplace, args=audioargs, check=False)
                videoargs = [
                    "youtube-dl",
                    "--add-metadata",
                    "-x",
                    "-q",
                    "--skip-unavailable-fragments",
                    "--console-title",
                    "-i",
                    "-o",
                    "%(title)s.%(ext)s",
                    "--restrict-filenames",
                    "--all-subs",
                    "-f",
                    videofiletype,
                    url,
                ]
                # print(args)
                # subprocess.run(cwd=place, args=args, check=True)
                subprocess.run(cwd=videoplace, args=videoargs, check=False)
            urlfile.truncate(0)
        time.sleep(10)
        # break


if __name__ == "__main__":
    main()
