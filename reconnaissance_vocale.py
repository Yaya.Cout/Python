#!/bin/python3
from speech_recognition import Microphone, Recognizer, UnknownValueError

recognizer = Recognizer()

# On enregistre le son

with Microphone() as source:
    print("Réglage du bruit ambiant... Patientez...")
    recognizer.adjust_for_ambient_noise(source)
    print("Vous pouvez parler...")
    recorded_audio = recognizer.listen(source)
    print("Enregistrement terminé !")
    with open("record.wav", "wb") as f:
        f.write(recorded_audio.get_wav_data())
# Reconnaissance de l'audio

try:
    print("Reconnaissance du texte...")
    googletext = recognizer.recognize_google(recorded_audio, language="fr-FR")
    print("Google pense que vous avez dit {}".format(googletext))
except UnknownValueError:
    print("L'audio n'as pas été compris par Google")
except Exception as ex:
    print(ex)

try:
    print("Reconnaissance du texte...")
    sphinxtext = recognizer.recognize_sphinx(recorded_audio, language="fr-FR")
    print("Sphinx pense que vous avez dit {}".format(sphinxtext))
except UnknownValueError:
    print("L'audio n'as pas été compris par Sphinx")
except Exception as ex:
    print(ex)
