import qwant
import sqlite3
import sys
# import ddg3

links = qwant.items("site:radiofrance-podcast.net", count=100)
# print(links)
# r = ddg3.query('site:radiofrance-podcast.net')
databasepath = "/home/neo/.var/app/org.gnome.Podcasts/data/gnome-podcasts/podcasts.db"
database = sqlite3.connect(databasepath)

if database.total_changes != 0:
    print("Error")
    sys.exit(1)

cursor = database.cursor()
# cursor.execute("INSERT INTO source VALUES ('Jamie', 'cuttlefish', 7)")
rows = cursor.execute("SELECT uri FROM source").fetchall()
# print(rows)

# links = [{"url": "https://radiofrance-podcast.net/podcast09/35099478-7c72-4f9e-a6de-1b928400e9e5/rss_13827.xml"}]
for item in links:
    url = item['url']
    print(url)

    query = "INSERT OR REPLACE INTO source(uri) VALUES('" + url + "')"
    cursor.execute(query)
